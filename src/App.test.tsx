import React from 'react';
import {
  render,
  screen,
  waitForElementToBeRemoved,
  within,
  fireEvent,
} from '@testing-library/react';

import App from './App';

const mockedUseNYTArticleSearch = jest.fn();
jest.mock('./hooks/useNYTArticleSearch.ts', () => ({
  useNYTArticleSearch: () => mockedUseNYTArticleSearch(),
}));

afterEach(() => mockedUseNYTArticleSearch.mockClear());
afterAll(() => jest.unmock('./hooks/useNYTArticleSearch.ts'));

describe('main App flows', () => {
  test('renders empty UI', () => {
    mockedUseNYTArticleSearch.mockReturnValue({
      isLoading: false,
      error: '',
      articles: [],
      pagesCount: 0,
      search: jest.fn(),
    });
    render(<App />);

    expect(screen.queryByTestId('results-wrapper')).toBeNull();
    expect(screen.queryByTestId('pagination-wrapper')).toBeNull();
  });

  test('renders loading screen', () => {
    mockedUseNYTArticleSearch.mockReturnValue({
      isLoading: true,
      error: '',
      articles: [],
      pagesCount: 0,
      search: jest.fn(),
    });
    render(<App />);

    expect(screen.getByTestId('loader')).toBeInTheDocument();
  });

  test('renders search results with pagination', async () => {
    const fixture = require('../test/fixtures/grover_p0.json');
    mockedUseNYTArticleSearch.mockReturnValue({
      isLoading: false,
      error: '',
      articles: fixture.response.docs,
      pagesCount: Math.ceil(fixture.response.meta.hits / 10),
      search: jest.fn(),
    });
    render(<App />);

    screen.getByText('A Big Tennis Tournament Is Happening in Miami. Really.');

    expect(screen.getByTestId('pagination-wrapper')).toBeInTheDocument();
  });

  test('calls search function with query param and default page', async () => {
    const search = jest.fn();
    mockedUseNYTArticleSearch.mockReturnValue({
      isLoading: false,
      error: '',
      articles: [],
      pagesCount: 0,
      search,
    });
    render(<App />);

    const searchInput = screen.getByTestId('search-input');
    fireEvent.change(searchInput, { target: { value: 'grover' } });
    fireEvent.submit(searchInput);

    expect(search.mock.calls).toContainEqual(['grover', 0]);
  });

  test('calls search function on page click', async () => {
    const fixture = require('../test/fixtures/grover_p0.json');
    const search = jest.fn();
    mockedUseNYTArticleSearch.mockReturnValue({
      isLoading: false,
      error: '',
      articles: fixture.response.docs,
      pagesCount: Math.ceil(fixture.response.meta.hits / 10),
      search,
    });
    render(<App />);

    const paginationWrapper = screen.getByTestId('pagination-wrapper');
    const p5button = within(paginationWrapper).getByText('5');
    fireEvent.click(p5button);

    expect(search.mock.calls).toContainEqual(['grover', 4]);
  });
});
