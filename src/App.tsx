import React from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

import { ArticlesList } from './Components/ArticlesList';
import { ArticleDetails } from './Components/ArticleDetails';

import { useNYTArticleSearch } from './hooks/useNYTArticleSearch';

function App() {
  // TODO: display err message
  const { isLoading, error, articles, pagesCount, search } =
    useNYTArticleSearch();
  return (
    <Router>
      <header className="bg-black p-2">
        <div className="text-white text-center">The NYT article search app</div>
      </header>

      <div className="container mx-auto p-4">
        <Switch>
          <Route path="/:urlQuery/:urlPage/:urlID" exact>
            <ArticleDetails
              articles={articles}
              search={search}
              isLoading={isLoading}
            />
          </Route>

          <Route path="/:urlQuery?/:urlPage?">
            <ArticlesList
              articles={articles}
              pagesCount={pagesCount}
              search={search}
              isLoading={isLoading}
            />
          </Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
