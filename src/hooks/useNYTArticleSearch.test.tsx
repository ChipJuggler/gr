import { renderHook, act } from '@testing-library/react-hooks';
import fetchMock from 'fetch-mock-jest';

afterEach(() => fetchMock.reset());

describe('useNYTArticleSearch hook tests', () => {
  test('fetches articles by query that is not in cache', async () => {
    let useNYTArticleSearch: any;
    jest.isolateModules(() => {
      ({ useNYTArticleSearch } = require('./useNYTArticleSearch'));
    });
    fetchMock.mock(
      'begin:https://api.nytimes.com/svc/search/v2/articlesearch.json',
      {
        status: 200,
        body: { response: { docs: [{}, {}, {}], meta: { hits: 45 } } },
      }
    );
    const { result, waitForValueToChange } = renderHook(() =>
      useNYTArticleSearch(100, 2, 2000)
    );

    expect(result.current.isLoading).toBe(false);
    expect(result.current.articles.length).toEqual(0);
    expect(result.current.pagesCount).toEqual(0);
    expect(fetchMock.called()).toBe(false);

    act(() => result.current.search('something', 0));

    await waitForValueToChange(() => result.current.isLoading);
    expect(result.current.articles.length).toEqual(3);
    expect(result.current.pagesCount).toEqual(5);
    expect(fetchMock.called()).toBe(true);
    expect(fetchMock.lastUrl()?.includes('?q=something&page=0')).toEqual(true);
  });

  test('fetches articles by page that is not in cache', async () => {
    let useNYTArticleSearch: any;
    jest.isolateModules(() => {
      ({ useNYTArticleSearch } = require('./useNYTArticleSearch'));
    });
    fetchMock.mock(
      'begin:https://api.nytimes.com/svc/search/v2/articlesearch.json',
      {
        status: 200,
        body: { response: { docs: [{}, {}, {}], meta: { hits: 45 } } },
      }
    );
    const { result, waitForValueToChange } = renderHook(() =>
      useNYTArticleSearch(100, 2, 2000)
    );

    act(() => result.current.search('something', 0));
    await waitForValueToChange(() => result.current.isLoading);
    act(() => result.current.search('something', 1));

    await waitForValueToChange(() => result.current.isLoading);
    expect(fetchMock.calls().length).toEqual(2);
    expect(fetchMock.lastUrl()?.includes('?q=something&page=1')).toEqual(true);
  });

  test('fetches articles that are in cache', async () => {
    let useNYTArticleSearch: any;
    jest.isolateModules(() => {
      ({ useNYTArticleSearch } = require('./useNYTArticleSearch'));
    });
    fetchMock.mock(
      'begin:https://api.nytimes.com/svc/search/v2/articlesearch.json',
      {
        status: 200,
        body: { response: { docs: [{}, {}, {}], meta: { hits: 45 } } },
      }
    );
    const { result, waitForValueToChange } = renderHook(() =>
      useNYTArticleSearch(100, 2, 2000)
    );

    act(() => result.current.search('something', 0));
    await waitForValueToChange(() => result.current.isLoading);
    act(() => result.current.search('something', 0));

    // should be 1, because of cache
    expect(fetchMock.calls().length).toEqual(1);
  });

  test('fetches articles that were wiped from cache', async () => {
    let useNYTArticleSearch: any;
    jest.isolateModules(() => {
      ({ useNYTArticleSearch } = require('./useNYTArticleSearch'));
    });
    fetchMock.mock(
      'begin:https://api.nytimes.com/svc/search/v2/articlesearch.json',
      {
        status: 200,
        body: { response: { docs: [{}, {}, {}], meta: { hits: 45 } } },
      }
    );
    const { result, waitForValueToChange } = renderHook(() =>
      useNYTArticleSearch(100, 2, 2000)
    );

    act(() => result.current.search('something', 0));
    await waitForValueToChange(() => result.current.isLoading);
    act(() => result.current.search('other', 0));
    await waitForValueToChange(() => result.current.isLoading);
    act(() => result.current.search('third thing', 0));
    await waitForValueToChange(() => result.current.isLoading);
    // should hit cache, no need to wait for isLoading
    act(() => result.current.search('other', 0));
    act(() => result.current.search('third thing', 0));
    act(() => result.current.search('something', 0));
    // 'something' should be wiped from cache already, so we need to wait
    await waitForValueToChange(() => result.current.isLoading);

    expect(fetchMock.calls().length).toEqual(4);
    expect(fetchMock.lastUrl()?.includes('?q=something&page=0')).toEqual(true);
  });
});
