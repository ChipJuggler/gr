import { useReducer, useEffect, useCallback } from 'react';

enum ACTION_TYPES {
  FETCH_ARTICLES = 'fetchArticles',
  IS_FETCHING_ARTICLES = 'isFetchingArticles',
  SUCCESS = 'fetchArticlesSuccess',
  FAILURE = 'fetchArticlesFailure',
}

export type Article = {
  _id: string;
  web_url: string;
  lead_paragraph: string;
  headline: {
    main: string;
  };
  pub_date: string;
};

type Action =
  | {
      type: ACTION_TYPES.FETCH_ARTICLES;
      payload: { query: string; page: number };
    }
  | { type: ACTION_TYPES.IS_FETCHING_ARTICLES }
  | {
      type: ACTION_TYPES.SUCCESS;
      payload: { articles: Article[]; articlesCount: number };
    }
  | { type: ACTION_TYPES.FAILURE };

type QueryResult = {
  timestamp: number;
  articlesCount: number;
  pages: Map<number, Article[]>;
};

type State = {
  query: string;
  page: number;
  isLoading: boolean;
  articles: Article[];
  articlesCount: number;
  error: string;
};

const _RPM = 10;
const _MAX_QUERIES = 30;
// 5 mins (query cache)
const _TTL = 1000 * 60 * 5;
// TODO: move to config
const API_KEY = 'WYW0qdZO7HtlQU1DTzqXuLZI8ceNVvba';

const initialState: State = {
  query: '',
  page: 0,
  isLoading: false,
  articles: [],
  articlesCount: 0,
  error: '',
};

const wait = async (ms: number) => await new Promise((r) => setTimeout(r, ms));

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case ACTION_TYPES.FETCH_ARTICLES:
      return {
        ...state,
        query: action.payload.query,
        page: action.payload.page,
        error: '',
      };
    case ACTION_TYPES.IS_FETCHING_ARTICLES:
      return {
        ...state,
        isLoading: true,
      };
    case ACTION_TYPES.SUCCESS:
      return {
        ...state,
        isLoading: false,
        articles: action.payload.articles,
        articlesCount: action.payload.articlesCount,
      };
    case ACTION_TYPES.FAILURE:
      return {
        ...state,
        isLoading: false,
        error: 'Fetch Err',
        lastReqAt: new Date().getTime(),
      };
    default:
      throw new Error();
  }
};

// could be changed to useRef if we won't need shared cache
const queriesCache: Map<string, QueryResult> = new Map();
let lastReqAt = 0;

export const useNYTArticleSearch = (
  // to simplify testing
  RPM = _RPM,
  MAX_QUERIES = _MAX_QUERIES,
  TTL = _TTL
) => {
  const [{ query, page, isLoading, articles, articlesCount, error }, dispatch] =
    useReducer(reducer, initialState);

  useEffect(() => {
    if (!query) {
      dispatch({
        type: ACTION_TYPES.SUCCESS,
        payload: {
          articles: [],
          articlesCount: 0,
        },
      });
      return;
    }
    if (page > 99) {
      dispatch({ type: ACTION_TYPES.FAILURE });
      return;
    }

    const cachedQuery = queriesCache.get(query);
    const cachedPage = cachedQuery?.pages.get(page);
    const cachedReqAt = queriesCache.get(query)?.timestamp;
    if (
      cachedQuery &&
      cachedPage &&
      cachedReqAt &&
      new Date().getTime() - cachedReqAt < TTL
    ) {
      dispatch({
        type: ACTION_TYPES.SUCCESS,
        payload: {
          articles: cachedPage,
          articlesCount: cachedQuery.articlesCount,
        },
      });
      return;
    }

    dispatch({ type: ACTION_TYPES.IS_FETCHING_ARTICLES });

    const fetchArticles = async (q: string, p: number) => {
      try {
        const lastReqMsDiff = new Date().getTime() - lastReqAt;
        if (lastReqMsDiff < (1000 * 60) / RPM) {
          await wait(lastReqMsDiff);
        }
        // TODO: filter out unneded fields from headline
        const res = await fetch(
          `https://api.nytimes.com/svc/search/v2/articlesearch.json?q=${q}&page=${p}&api-key=${API_KEY}&fq=document_type:article&fl=web_url,headline,lead_paragraph,_id,pub_date`
        );
        lastReqAt = new Date().getTime();
        const {
          response: {
            docs: newArticles,
            meta: { hits: newArticlesCount },
          },
        } = await res.json();

        dispatch({
          type: ACTION_TYPES.SUCCESS,
          payload: {
            articles: newArticles,
            articlesCount: newArticlesCount,
          },
        });

        if (!cachedQuery) {
          queriesCache.set(query, {
            timestamp: new Date().getTime(),
            articlesCount: newArticlesCount,
            pages: new Map([[page, newArticles]]),
          });
        } else {
          if (
            newArticlesCount !== cachedQuery.articlesCount ||
            new Date().getTime() - cachedQuery.timestamp > TTL
          ) {
            // so the new data will always be in the end, it will simplify
            // cache cleanup
            queriesCache.delete(query);
            queriesCache.set(query, {
              timestamp: new Date().getTime(),
              articlesCount: newArticlesCount,
              pages: new Map([[page, newArticles]]),
            });
          } else {
            const newPages = new Map(cachedQuery.pages);
            newPages.set(page, newArticles);
            // so the new data will always be in the end, it will simplify
            // cache cleanup
            queriesCache.delete(query);
            queriesCache.set(query, {
              timestamp: cachedQuery.timestamp,
              articlesCount: cachedQuery.articlesCount,
              pages: newPages,
            });
          }
        }
        // cache cleanup
        if (queriesCache.size > MAX_QUERIES) {
          Array.from(queriesCache.keys())
            .slice(0, queriesCache.size - MAX_QUERIES)
            .forEach((key) => queriesCache.delete(key));
        }
      } catch (e) {
        // TODO: check throttle err code and call fetchArticles again
        console.log(e);
        dispatch({ type: ACTION_TYPES.FAILURE });
      }
    };

    fetchArticles(query, page);
  }, [query, page]);

  return {
    isLoading,
    error,
    articles,
    pagesCount: Math.ceil(articlesCount / 10),
    search: useCallback(
      (q: string, p: number) =>
        dispatch({
          type: ACTION_TYPES.FETCH_ARTICLES,
          payload: { query: q, page: p },
        }),
      []
    ),
  };
};
