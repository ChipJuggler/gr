import React, { useState, useEffect } from 'react';
import ReactPaginate from 'react-paginate';
import { useParams, useHistory, Link } from 'react-router-dom';

import { Loader } from '../Loader';

import { Article } from '../../hooks/useNYTArticleSearch';

import './ArticlesList.css';

export const ArticlesList = ({
  articles,
  pagesCount,
  search,
  isLoading,
}: {
  articles: Article[];
  pagesCount: number;
  search: (q: string, p: number) => void;
  isLoading: boolean;
}) => {
  const history = useHistory();
  const { urlQuery, urlPage } =
    useParams<{ urlQuery: string | undefined; urlPage: string | undefined }>();
  const [query, setQuery] = useState(decodeURIComponent(urlQuery || ''));

  useEffect(() => {
    search(decodeURIComponent(urlQuery || ''), Number(urlPage) || 0);
  }, [urlQuery, urlPage, search]);

  return (
    <>
      <section>
        <form
          onSubmit={(e) => {
            e.preventDefault();
            history.push(`/${encodeURIComponent(query)}`);
          }}
        >
          <input
            data-testid="search-input"
            className="search-input outline-none border-2 border-r-0 rounded-tl-md rounded-bl-md p-2"
            placeholder="search"
            value={query}
            onChange={({ target: { value } }) => setQuery(value)}
            type="search"
          />
          <button
            className="border-2 border-l-0 rounded-tr-md rounded-br-md p-2"
            type="submit"
            disabled={isLoading}
          >
            🔎
          </button>
        </form>
      </section>
      {isLoading && <Loader />}
      {pagesCount > 100 && (
        <div className="bg-yellow-500 rounded-md bg-opacity-50 p-2 my-2">
          Too many results :( Please, be more specific. Displaying first 100
          pages only
        </div>
      )}

      {Boolean(articles.length) && (
        <>
          <section data-testid="results-wrapper" className="flex flex-col mb-6">
            <h1 className="font-bold text-lg mt-6 mb-2">Results:</h1>
            {articles.map((article) => (
              <div
                className="flex flex-col border-b border-gray-200 border-solid hover:shadow-md rounded my-2 p-4"
                key={article._id}
              >
                <Link
                  className="text-blue-900 hover:text-blue-500"
                  to={`/${urlQuery}/${urlPage || 0}/${encodeURIComponent(
                    article._id
                  )}`}
                >
                  {article.headline.main}
                </Link>
              </div>
            ))}
          </section>

          <section data-testid="pagination-wrapper">
            <ReactPaginate
              pageCount={pagesCount > 100 ? 100 : pagesCount}
              forcePage={Number(urlPage) || 0}
              pageRangeDisplayed={5}
              marginPagesDisplayed={3}
              onPageChange={({ selected }) =>
                history.push(`/${urlQuery}/${selected}`)
              }
              containerClassName="p-4"
              disabledClassName="text-gray-300 p-1 hover:shadow-none"
              nextClassName="inline-block p-1 hover:shadow-md rounded"
              previousClassName="inline-block p-1 hover:shadow-md rounded"
              pageClassName="inline-block p-1 hover:shadow-md rounded"
              breakClassName="inline-block p-1 hover:shadow-md rounded"
              activeClassName="active text-yellow-400 font-bold border border-gray-200 border-solid hover:shadow-md rounded"
            />
          </section>
        </>
      )}
    </>
  );
};
