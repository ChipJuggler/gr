import React from 'react';

import './Loader.css';

export const Loader = () => (
  <div data-testid="loader" className="lds-dual-ring" />
);
