import React, { useEffect } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import formatDate from 'date-fns/format';

import { Loader } from '../Loader';

import { Article } from '../../hooks/useNYTArticleSearch';

export const ArticleDetails = ({
  articles,
  search,
  isLoading,
}: {
  articles: Article[];
  search: (q: string, p: number) => void;
  isLoading: boolean;
}) => {
  const history = useHistory();
  const { urlQuery, urlPage, urlID } =
    useParams<{ urlQuery: string; urlPage: string; urlID: string }>();

  const article = articles.find((a) => a._id === decodeURIComponent(urlID));

  useEffect(() => {
    if (!article) {
      search(urlQuery, Number(urlPage));
    }
  }, [urlID, urlQuery, urlPage, search, article]);

  return (
    <section>
      {isLoading && <Loader />}
      {article && (
        <>
          <button
            className="text-blue-900 hover:text-blue-500"
            onClick={() => history.goBack()}
          >
            {'<'} Go to previous page
          </button>
          <div>
            <h1 className="font-bold text-lg mt-6 mb-2">
              {article.headline.main}
            </h1>
            <div className="font-medium">
              {formatDate(new Date(article.pub_date), 'dd.MM.yyyy')}
            </div>
            <div className="my-4">{article.lead_paragraph}</div>
          </div>
          <a
            className="text-blue-900 hover:text-blue-500"
            href={article.web_url}
            target="_blank"
            rel="noreferrer"
          >
            Read the full story
          </a>
        </>
      )}
    </section>
  );
};
