# NYT article search

run: `yarn start`

test: `yarn test`

build: `yarn build`

## How this app was designed

The app is too small to introduce any state management library aside from `useReducer`.

All fetching/state logic is locaed inside the `useNYTArticleSearch` hook.

NYT api has restrictions (10 RPM, 4000 RPD), so a simple cache were added (and a basic throttling). The caching strategy is simple: store qeury pages for a few minutes and wipe "old" queries if there are too many in storage. This is covering the main usage flow. Ofc there are some edge cases (query result can change between reqs), so a basic safety measure was added: if the number of results for a query is different for different page requests, then the whole query is deleted from a storage.

## To improve

* UI styling
* Err handling/displaying
* Smarter caching
* more tests